class TheDink {
    constructor(id, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
}

let TRA_TAT = new TheDink( "1", "TRATAC", "Trà tắc", "10000", "14/5/2021", "14/5/2021");
let COCA = new TheDink(  "2", "COCA", "Cocacola", "15000", "14/5/2021", "14/5/2021");
let PEPSI = new TheDink( "3", "PEPSI", "Pepsi", "15000", "14/5/2021", "14/5/2021");

let drinks_class = [];
drinks_class.push(TRA_TAT, COCA, PEPSI);

var drinks_object = [
    {"id": "1",
    "maNuocUong": "TRATAC",
    "tenNuocUong": "Trà tắc",
    "donGia": "10000",
    "ngayTao": "14/5/2021",
    "ngayCapNhat": "14/5/2021"},
    {"id": "2",
    "maNuocUong": "COCA",
    "tenNuocUong": "Cocacola",
    "donGia": "15000",
    "ngayTao": "14/5/2021",
    "ngayCapNhat": "14/5/2021"},
    {"id": "3",
    "maNuocUong": "PEPSI",
    "tenNuocUong": "Pepsi",
    "donGia": "15000",
    "ngayTao": "14/5/2021",
    "ngayCapNhat": "14/5/2021"}
]


//import thu vien express js 
const express = require('express');



//Khoi tao app Express 
const app = express();

//Khai bao cong chay project
const port = 3000;

//Callback function la mot function tham so cura mot function khac, no se thuc hien khi function day duoc goi
//Khai bao api dang /

//Khai bao api dang GET
app.get("/get-method" , (req,res) => {
    res.json ({
        message: drinks_object
    })
})




app.listen(port, () => {
    console.log('App listening on port', port);
})